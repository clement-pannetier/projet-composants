# Projet Composants

BookFinder est une application permettant de rechercher des livres et de les filtrer en fonction de leur prix.
Ces livres sont stockés sous la forme d'un fichier json puis insérés dans une base de données couchDB.
Lorsque les livres sont insérés dans celle-ci, le fichier json n'est plus nécessaire.
Cette application utilise les composants web avec la version 2 de Polymer et reprend le thème Material Design de Google.

## Informations

- On peut rechercher un livre avec la barre de recherche située en haut
- On peut filtrer les livres en fonction de leur prix
- Un livre est défini dans une paper-card avec sa couverture, titre, auteur et prix
- Un bouton permet d'aller sur le site marchand pour acheter le livre

## Installation / Lancement

Clonez ce repository :

```shell
git clone https://gitlab.com/clement.pannetier/projet-composants.git
```

### Avec Docker (Sur le localhost)

```shell
cd projet-composants
```

```shell
docker-compose up -d
```

```shell
docker cp projet-composants_node_1:/home/user/bower_components ./src/
```

Ouvrir cette URL sur votre navigateur : [http://localhost:8080](http://localhost:8080).

Si vous souhaitez accéder à l'interface de CouchDB pour voir le contenu de la BDD : [http://localhost:5984/_utils](http://localhost:5984/_utils).

### Normale

```shell
cd projet-composants/src
```

```shell
bower install
```

```shell
python3 -m http.server 9999
```

Ouvrir cette URL sur votre navigateur : [http://localhost:9999](http://localhost:9999).

## Dépendances (Bower) / Composants utilisés

- Polymer/polymer#^2.0.0
- PolymerElements/app-layout#^2.0.0
- PolymerElements/paper-card#^2.0.0
- PolymerElements/paper-button#^2.0.0
- paper-search#^2.0.9
- pouchdb#^7.0.0"
