FROM node:10.15.0-alpine

RUN npm install -g bower
RUN apk add git

RUN mkdir /home/user
WORKDIR /home/user

COPY ./src .
RUN bower install -allow-root